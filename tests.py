import json
import unittest

from i7extract import InformText, extract_texts, standard_subs


class InformTextTest(unittest.TestCase):
    """The tests for the InformText class."""

    def test_text_with_quote(self):
        """A InformText with a quotation mark should raise an error."""

        self.assertRaises(ValueError, InformText, 'Hello " World')

    def test_text_with_wrong_brackets(self):
        """An InformText with misplaced brackets should raise an error."""

        # An opening bracket in a substitution.
        self.assertRaises(ValueError, InformText, "Hello [substi[tution] World")
        # A closing bracket outside of a substitution.
        self.assertRaises(ValueError, InformText, "Hello [substitution] ] World")
        # A text ending with an unclosed substitution.
        self.assertRaises(ValueError, InformText, "Hello [substitution World")

    def test_tokenisation(self):
        """An InformText should be correctly tokenised (text and substitutions separated)."""

        # A text without substitutions.
        text = InformText("Hello World")
        self.assertListEqual(text.tokens, ["Hello World"])
        # A text that is only a substitution
        text = InformText("[Hello World]")
        self.assertListEqual(text.tokens, ["[Hello World]"])
        # A text that starts with a substitution.
        text = InformText("[Hello] World")
        self.assertListEqual(text.tokens, ["[Hello]", " World"])
        # A text that ends with a substitution.
        text = InformText("Hello [World]")
        self.assertListEqual(text.tokens, ["Hello ", "[World]"])
        # A text that has a substitution in the middle.
        text = InformText("Hello [substitution] World")
        self.assertListEqual(text.tokens, ["Hello ", "[substitution]", " World"])
        # A text that has several substitutions
        text = InformText("Hello [first] [second] World")
        self.assertListEqual(text.tokens, ["Hello ", "[first]", " ", "[second]", " World"])
        # A text that starts and en with a substitution.
        text = InformText("[Hello] Center [World]")
        self.assertListEqual(text.tokens, ["[Hello]", " Center ", "[World]"])
        # A text that is only several substitutions.
        text = InformText("[Hello][World]")
        self.assertListEqual(text.tokens, ["[Hello]", "[World]"])

    def test_substitutions(self):
        """An InformText should substitute correctly, given substitutions rules."""

        # A list containing each type of substitution.
        subs = [
            {
                "is": "line break",
                "replacement": "\n"
            },
            {
                "startswith": "the ",
                "replacement": "the {}"
            },
            {
                "startswith": "unicode ",
                "unicode": "{}"
            }
        ]
        subs_overwrite = [
            {
                "is": "line break",
                "replacement": "overwritten"
            }
        ]
        # A standard replacement substitution.
        text = InformText("Hello[line break]World")
        self.assertEqual(text.substitute(subs), "Hello\nWorld")
        # A replacement from a startswith substitution.
        text.text = "Hello [the World]"
        self.assertEqual(text.substitute(subs), "Hello the World")
        # Unicode substitutions.
        text.text = "H[unicode 101]llo W[unicode latin small letter o]rld"
        self.assertEqual(text.substitute(subs), "Hello World")
        # An unspecified substitution.
        text.text = "Hello [nonexistent] World"
        self.assertEqual(text.substitute(subs), "Hello [nonexistent] World")
        # A text with multiple substitutions.
        text.text = "H[unicode 101]llo[line break][the World]"
        self.assertEqual(text.substitute(subs), "Hello\nthe World")
        # Overwriting a substitution.
        text.text = "Hello [line break] World"
        self.assertEqual(text.substitute(subs, subs_overwrite), "Hello overwritten World")


class ExtractTextsTest(unittest.TestCase):
    """The tests for the extract_texts function."""

    def test_simple_source(self):
        source = """
            "Test" By Nathanaël Marion

            There is a room. "It's a test room."
        """
        expected = [InformText("Test"), InformText("It's a test room.")]
        self.assertListEqual(extract_texts(source), expected)

    def test_source_with_comments(self):
        source = """
            "Test"

            [A comment! "I will not be extracted!"[A [nested "me neither"] comment.]]

            There is a room. "It's a test room."
        """
        expected = [InformText("Test"), InformText("It's a test room.")]
        self.assertListEqual(extract_texts(source), expected)

    def test_source_with_substitutions(self):
        source = """
            "Test"

            There is a room. "It's a [test] room."
        """
        expected = [InformText("Test"), InformText("It's a [test] room.")]
        self.assertListEqual(extract_texts(source), expected)


class SubstitutionsTest(unittest.TestCase):
    """The tests that check every built-in substitution."""

    def test_standard(self):
        text = InformText("")
        text.text = "[bold type]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[italic type]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[roman type]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[fixed letter spacing]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[variable letter spacing]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[']"
        self.assertEqual(text.substitute(standard_subs), "'")
        text.text = "[line break]"
        self.assertEqual(text.substitute(standard_subs), "\n")
        text.text = "[no line break]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[paragraph break]"
        self.assertEqual(text.substitute(standard_subs), "\n\n")
        text.text = "[run paragraph on]"
        self.assertEqual(text.substitute(standard_subs), "")
        text.text = "[command clarification break]"
        self.assertEqual(text.substitute(standard_subs), "\n")
        text.text = "[here]"
        self.assertEqual(text.substitute(standard_subs), "here")
        text.text = "[Here]"
        self.assertEqual(text.substitute(standard_subs), "Here")
        text.text = "[now]"
        self.assertEqual(text.substitute(standard_subs), "now")
        text.text = "[Now]"
        self.assertEqual(text.substitute(standard_subs), "Now")
        text.text = "[bracket]"
        self.assertEqual(text.substitute(standard_subs), "[")
        text.text = "[close bracket]"
        self.assertEqual(text.substitute(standard_subs), "]")
        text.text = "[the noun]"
        self.assertEqual(text.substitute(standard_subs), "the noun")
        text.text = "[The noun]"
        self.assertEqual(text.substitute(standard_subs), "The noun")
        text.text = "[a noun]"
        self.assertEqual(text.substitute(standard_subs), "a noun")
        text.text = "[A noun]"
        self.assertEqual(text.substitute(standard_subs), "A noun")
        text.text = "[unicode 363]"
        self.assertEqual(text.substitute(standard_subs), "ū")
        text.text = "[unicode Latin Small Letter U with Macron]"
        self.assertEqual(text.substitute(standard_subs), "ū")

    def test_french(self):
        text = InformText("")
        with open("./substitutions/french.json", encoding="utf8") as f:
            french_subs = json.load(f)
        text.text = "[gras]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[italique]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[romain]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[largeur fixe]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[largeur variable]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[--]"
        self.assertEqual(text.substitute(french_subs), "—")
        text.text = "[_]"
        self.assertEqual(text.substitute(french_subs), " ")
        text.text = "[à la ligne]"
        self.assertEqual(text.substitute(french_subs), "\n")
        text.text = "[sans passage à la ligne]"
        self.assertEqual(text.substitute(french_subs), "")
        text.text = "[saut de paragraphe]"
        self.assertEqual(text.substitute(french_subs), "\n\n")
        text.text = "[ici]"
        self.assertEqual(text.substitute(french_subs), "ici")
        text.text = "[Ici]"
        self.assertEqual(text.substitute(french_subs), "Ici")
        text.text = "[maintenant]"
        self.assertEqual(text.substitute(french_subs), "maintenant")
        text.text = "[crochet]"
        self.assertEqual(text.substitute(french_subs), "[")
        text.text = "[crochet fermant]"
        self.assertEqual(text.substitute(french_subs), "]")
        text.text = "[le noun]"
        self.assertEqual(text.substitute(french_subs), "le noun")
        text.text = "[Le noun]"
        self.assertEqual(text.substitute(french_subs), "Le noun")
        text.text = "[la noun]"
        self.assertEqual(text.substitute(french_subs), "la noun")
        text.text = "[La noun]"
        self.assertEqual(text.substitute(french_subs), "La noun")
        text.text = "[les nouns]"
        self.assertEqual(text.substitute(french_subs), "les nouns")
        text.text = "[Les nouns]"
        self.assertEqual(text.substitute(french_subs), "Les nouns")
        text.text = "[un noun]"
        self.assertEqual(text.substitute(french_subs), "un noun")
        text.text = "[Un noun]"
        self.assertEqual(text.substitute(french_subs), "Un noun")
        text.text = "[une noun]"
        self.assertEqual(text.substitute(french_subs), "une noun")
        text.text = "[Une noun]"
        self.assertEqual(text.substitute(french_subs), "Une noun")
        text.text = "[des nouns]"
        self.assertEqual(text.substitute(french_subs), "des nouns")
        text.text = "[Des nouns]"
        self.assertEqual(text.substitute(french_subs), "Des nouns")
        text.text = "[du noun]"
        self.assertEqual(text.substitute(french_subs), "du noun")
        text.text = "[Du noun]"
        self.assertEqual(text.substitute(french_subs), "Du noun")
        text.text = "[de la noun]"
        self.assertEqual(text.substitute(french_subs), "de la noun")
        text.text = "[De la noun]"
        self.assertEqual(text.substitute(french_subs), "De la noun")
        text.text = "[au noun]"
        self.assertEqual(text.substitute(french_subs), "au noun")
        text.text = "[Au noun]"
        self.assertEqual(text.substitute(french_subs), "Au noun")
        text.text = "[à la noun]"
        self.assertEqual(text.substitute(french_subs), "à la noun")
        text.text = "[À la noun]"
        self.assertEqual(text.substitute(french_subs), "À la noun")
        text.text = "[aux nouns]"
        self.assertEqual(text.substitute(french_subs), "aux nouns")
        text.text = "[Aux nouns]"
        self.assertEqual(text.substitute(french_subs), "Aux nouns")


if __name__ == "__main__":
    unittest.main()
