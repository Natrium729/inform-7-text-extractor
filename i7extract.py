"""This script extracts the texts from an Inform 7 source and substitutes them when possible.

For usage, see README.md or type $ python i7extract.py -h

Author: Nathanaël Marion <natrium729@gmail.com>
Repository: https://bitbucket.org/Natrium729/inform-7-text-extractor
Licence: Public domain
"""

import argparse
import json
import os
import sys
import unicodedata


class InformText:
    """Represent an Inform text.

    An InformText can show its unsubstituted contents and has methods to parse and to substitute it.
    """

    def __init__(self, text: str):
        """Creates an Inform text with the given string."""

        if '"' in text:
            raise ValueError("An Inform text cannot contain quotations marks.")
        self.text = text

    def __repr__(self):
        return "i7extract.InformText(" + repr(self.text) + ")"

    def __str__(self):
        return self.text

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return self.text == other.text
        else:
            return False

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, text):
        self.__text = text
        self.__tokenise()

    def __tokenise(self):
        """Tokenise the InformText, creating a list of strings, separating run of normal texts and substitutions."""

        in_brackets = False
        tokens = []
        token_so_far = ""
        for i in range(len(self.text)):
            if self.text[i] == "[":
                if in_brackets:
                    raise ValueError("Inform text substitutions cannot contain brackets.")
                else:
                    in_brackets = True
                    if token_so_far:
                        tokens.append(token_so_far)
                    token_so_far = "["
            elif self.text[i] == "]":
                if in_brackets:
                    in_brackets = False
                    token_so_far += self.text[i]
                    tokens.append(token_so_far)
                    token_so_far = ""
                else:
                    raise ValueError("An Inform text cannot contain a closing bracket outside of text subsitutions.")
            else:
                token_so_far += self.text[i]
        if in_brackets:
            raise ValueError("An Inform text cannot end with an unclosed text substitution.")
        if token_so_far:
            tokens.append(token_so_far)
        self.tokens = tokens

    def substitute(self, *args):
        substituted_text = ""
        subs = []
        for arg in reversed(args):
            subs += arg

        for token in self.tokens:
            if token[0] == "[":
                sub_contents = token[1:-1]
                matched = False
                rest = None
                for sub in subs:
                    if "is" in sub and sub["is"] == sub_contents:
                        matched = True
                    elif "startswith" in sub and sub_contents.startswith(sub["startswith"]):
                        matched = True
                        rest = sub_contents[len(sub["startswith"]):]
                    if matched:
                        if "replacement" in sub:
                            replacement = sub["replacement"]
                            if rest is not None and "{}" in replacement:
                                replacement = replacement.format(rest)
                            substituted_text += replacement
                        elif "unicode" in sub:
                            if rest is not None:
                                if rest.isdigit():
                                    substituted_text += chr(int(rest))
                                else:
                                    substituted_text += unicodedata.lookup(rest.strip())
                        else:
                            raise ValueError("The substitution {} does not specifies how it should be dealt with.".format(sub))
                        break
                if not matched:
                    substituted_text += token
            else:
                substituted_text += token

        return substituted_text


def extract_texts(source: str):
    """Extracts and returns the texts from an Inform source."""

    strings = []
    current_string_start = None
    in_comment = 0

    for i in range(len(source)):
        char = source[i]
        if current_string_start is None:
            if char == "[":
                in_comment += 1
            elif char == "]":
                in_comment -= 1
            elif not in_comment and char == '"':
                current_string_start = i + 1
        elif not in_comment and char == '"':
            current_string = InformText(source[current_string_start:i])
            strings.append(current_string)
            current_string_start = None

    return strings


with open("./substitutions/standard.json", encoding="utf8") as f:
    standard_subs = json.load(f)


def main(args=None):
    """The command line interface, where args contains the command line arguments."""

    parser = argparse.ArgumentParser(description="extract the text of an Inform project or extension, and substitute it")
    parser.add_argument("path", help="the path of the Inform project or extension")
    parser.add_argument("-u", "--usesubs", help="use a substitution specification stored in a JSON file in the 'substitutions' folder (exemple: '-u french')", nargs="*", default=[])
    parser.add_argument("-n", "--nosubs", help="keep the extracted text unsubstituted", action="store_true")
    parser.add_argument("-o", "--output", help="the name of the file that will contain the extracted text")
    parser.add_argument("-s", "--sep", help="The string that will separate each text in the output file (default: ===)", default="===")
    parser.add_argument("-q", "--quiet", help="don't print the progress", action="store_true")
    args = parser.parse_args(args)

    if not args.quiet:
        print("Inform 7 text extractor\n")

    base_name = os.path.splitext(os.path.basename(args.path))
    if args.output is None:
        args.output = base_name[0] + "_texts.txt"
    elif not os.path.splitext(args.output)[-1]:
        args.output += ".txt"

    if os.path.splitext(args.path)[-1] == ".inform":
        args.path = os.path.join(args.path, "Source/story.ni")

    if "/" in args.output or "\\" in args.output:
        sys.exit("The output file cannot contain the characters '/' or '\\'")

    if not args.quiet:
        print("Opening the source file...")
    try:
        with open(args.path, encoding="utf8") as f:
            source = f.read()
    except OSError as e:
        sys.exit("The file at {} could not be opened.\n{}".format(args.path, e))

    if not args.quiet:
        print("Extracting the texts from the source...")
    texts = extract_texts(source)

    if args.nosubs:
        for i in range(len(texts)):
            texts[i] = texts[i].text
    else:
        if not args.quiet:
            print("Loading the substitution specifications...")
        used_subs = [standard_subs]
        for subs in args.usesubs:
            path = os.path.join("./substitutions", subs + ".json")
            try:
                with open(path, encoding="utf8") as f:
                    used_subs.append(json.load(f))
            except OSError as e:
                sys.exit("The substitution file {} could not be opened.\n{}".format(subs + ".json", e))
        if not args.quiet:
            print("Substituting the texts...")
        for i in range(len(texts)):
            texts[i] = texts[i].substitute(*used_subs)

    if not args.quiet:
        print("Writing the output file...")
    args.sep = "\n\n" + args.sep + "\n\n"
    try:
        with open(args.output, "w", encoding="utf8") as f:
            f.write(args.sep.join(texts))
    except OSError as e:
        sys.exit("The output file could not be written.\n{}".format(e))

    if not args.quiet:
        print("Done!")


if __name__ == "__main__":
    main()
