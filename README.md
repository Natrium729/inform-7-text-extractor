# Inform 7 Text Extractor

This is a Python 3 script that extracts all the text from an Inform 7 source so that one can easily proofread it. Additionally, it tries to substitute as far as possible the text substitutions so that spell checkers are not troubled by the brackets.

# Usage

The simplest way to use is by executing the script with a path pointing to an Inform file (or any text file containing an Inform source).

```shell
$ python i7extract.py MyStory.inform
```

A file containing the extracted text and called `MyStory_texts.txt` will be created in the same directory as the script. If `MyStory_texts.txt` already exists, it will be overwritten, so beware.

# Options

## Displaying help

The `--help` option (`-h` for short) displays succinct instructions on how to use the script.

```shell
$ python i7extract.py -h
```

The details are written below.

## Making no substitutions

The `--nosubs` option (`-n` for short) makes the script extract the texts without performing substitutions.

```shell
$ python i7extract.py MyStory.inform -n
```

## Changing the name of the output file

It is possible to change the name of the file that will contain the extracted texts with the `--output` option (`-o` for short).

```shell
$ python i7extract.py MyStory.inform -o my-texts.txt
```

In the example above, the texts will be written in a file named `my-texts.txt`.

If no extension is provided, the script will use `.txt`.

If a file with the specified name already exists, it will be overwritten, so be careful not to use the name of a file you need.

## Changing the string separating the extracted texts

By default, the extracted texts will be separated in by a row of three `=` signs in the output text.

For example, the following source:

```inform7
"My Story"

The hall is a room. "You stand in a huge hall."
```

will give the following file:

```
My Story

===

You stand in a huge hall.
```

We can change the separator with the `--sep` option (`-s` for short). For example, to use the string `[SEPARATOR]`:

```shell
$ python i7extract.py MyStory.inform -s [SEPARATOR]
```

## Using custom substitutions

It is possible to add custom substitutions by adding a JSON file to the `substitutions` folder (its format is explained below).

If we wrote a `custom.json` file, we could use it by using the `--usesubs` option (`-u` for short).

```shell
$ python i7extract.py MyStory.inform -u custom
```

It is possible to use multiple substitution files at the same time:

```shell
$ python i7extract.py MyStory.inform -u french custom
```

will use the `french.json` and the `custom.json` files.

The substitution file for the Standard Rules is always used, so we don't have to write a JSON for them. Included in this repository is a file for the French substitutions.

## Not displaying progress

The script prints its progress in the console. It is possible to suppress this behaviour by using the `--quiet` option (`-q` for short).

```shell
$ python i7extract.py MyStory.inform -q
```

## Using multiple options

It is possible to use multiple options, of course:

```shell
$ python i7extract.py MyStory.inform -u custom french -o extracted.txt -s -----
```

If `--nosubs` is used, then `--usesubs` does nothing.

# Writing a substitution file

A substitution file is a simple JSON file specifying how the script should handle substitutions. The file should contain an array of objects, each one representing a substitution. For example:

```json
[
    {
        "is": "my substitution",
        "replacement": "substituted!"
    }
]
```

With the above, `[my substitution]` will be replaced by `substituted!` in extracted texts.

## Matching a substitution

Every object should have either an `"is"` or a `"startswith"` key. A substitution will match with the object if its contents are the same as the `"is"` property or if its contents start with the `startswith` property.

Let's consider the following, for instance:

```json
[
    {
        "is": "hello"
    },
    {
        "startswith": "greet "
    }
]
```

Here, the first object will match with the substitution `[hello]`. The second will match with `[greet the player]`, `[greet lamp]` and any substitution beginning with `greet `.

However, as it is, the JSON will cause an error because it does not tell how to handle the substitutions.

## Handling a substitution

The `"replacement"` is used to tell by what text a substitution should be replaced. If the substitution has been matched with the `"startswith"` property, then we can use `{}` to complete with the rest of the substitution.

```json
[
    {
        "is": "hello",
        "replacement": "Hello World"
    },
    {
        "startswith": "greet ",
        "replacement": "Hello {}"
    }
]
```

In the above, the first object says that the substitution `[hello]` will be replaced with `Hello World`. The second says that the substitution `[greet the player]` will be replaced by `Hello the player`, `[greet lamp]` by `Hello lamp`, and so on.

## Overwriting a substitution

When using multiple substitution files, a later one can overwrite earlier ones.

```shell
$ python i7extract.py MyStory.inform -u french custom
```

With the above, a substitution specified in `custom.json` can overwrite substitutions in `french.json` and in `standard.json`.

# Substitutions that are not handled

This script does not handle segmented substitutions (`if`, `one of`…) and substitutions for adaptive texts (verbs, pronouns, `regarding (object)`…)

# Bugs reports and feature requests

Please use the bug tracker (link in the menu on the left).

# Licence

Public domain.
